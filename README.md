# centos-php-nginx
Dockerfile to create an image with CentOS, PHP 8.2 and Nginx.

1. Use CentOS Base Image
2. Install PHP 8 and it's dependencies
3. Install Composer
4. Start Nginx and PHP-FPM

```
# command to build a new image
docker build -t centos-php-nginx:v1 .
```

```
# command to run the new image
docker run -d -p 8080:8080 --name frontend centos-php-nginx:v1
```

```
# command to check logs
docker logs frontend
```

### Pushing to Docker Hub Registry

```
# login command
docker login

Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: pratyush.majumdar@gmail.com
Password: 
Login Succeeded
```

```
# example command to create a tag
docker tag local-image:tagname new-repo:tagname

# command to create a tag
docker tag centos-php-nginx:v1 prat1980/centos-php-nginx:v1

# example command to push a repository
docker push new-repo:tagname

# command to push a repository
docker push prat1980/centos-php-nginx:v1
```

### Deleting local repository (cleanup)
Deletion of a Docker image that is referenced in multiple repositories.

```
# delete the referenced image
docker rmi prat1980/centos-php-nginx:v1

# finally delete the original image
docker rmi centos-php-nginx:v1
```