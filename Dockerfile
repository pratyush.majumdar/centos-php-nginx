# Use CentOS 7 as the base image
FROM centos:7

# Set environment variables
ENV PHP_VERSION 8.2

# Install epel-release and other dependencies
RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install wget yum-utils && \
    yum -y install nginx

# Add the Remi repository and install PHP 8.2 and PHP-FPM
RUN yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
    yum-config-manager --enable remi-php82 && \
    yum -y install php php-fpm php-cli php-mysqlnd php-json php-opcache php-xml php-mbstring

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

# Configure PHP-FPM
RUN sed -i 's/user = apache/user = nginx/' /etc/php-fpm.d/www.conf && \
    sed -i 's/group = apache/group = nginx/' /etc/php-fpm.d/www.conf && \
    sed -i 's/;listen.owner = nobody/listen.owner = nginx/' /etc/php-fpm.d/www.conf && \
    sed -i 's/;listen.group = nobody/listen.group = nginx/' /etc/php-fpm.d/www.conf

# Ensure /run/php-fpm directory exists
RUN mkdir -p /run/php-fpm && chown nginx:nginx /run/php-fpm

# Copy the Nginx configuration file
COPY nginx.conf /etc/nginx/nginx.conf

# Copy index.php to the web root
COPY index.php /usr/share/nginx/html/

# Expose port 8080
EXPOSE 8080

# Start PHP-FPM and Nginx
CMD ["sh", "-c", "php-fpm && nginx -g 'daemon off;'"]
